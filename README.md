# Preserving Processing Instructions & Comments

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Compare release. The resources should be located such that they are two levels below the top level release directory.*

*For example `DeltaXML-XML-Compare-10_0_0_n/samples/preserving-processing-instructions-and-comments`.*

---

How to convert processing instructions and comments into XML markup (and back again) so that they are not removed by XML Compare.

This document describes how to run the sample. For concept details see: [Preserving Processing Instructions and Comments](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/preserving-processing-instructions-and-comments)

## Running the Sample

The sample can be run via the *run.bat* batch file, so long as this is issued from the sample directory. Alternatively, as this batch file contains a single command, the command can be executed directly:

	..\..\bin\deltaxml.exe compare preserve input1.xml input2.xml result.xml
	
# **Note - .NET support has been deprecated as of version 10.0.0 **